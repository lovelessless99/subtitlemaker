/**
 * Copyright 2017 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package recognize;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import com.ibm.watson.developer_cloud.http.HttpMediaType;
import subtitleMaker.FileUpload;
import subtitleMaker.FileUpload.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import recognize.model.RecognizeOptions;
import recognize.model.SpeechAlternative;
import recognize.model.SpeechResults;
import recognize.model.Transcript;
import recognize.websocket.BaseRecognizeCallback;
import subtitleMaker.LoginIBMPage;

/**
 * Recognize a sample wav file and print the transcript into the console output.
 * Make sure you are using UTF-8 to print messages; otherwise, you will see
 * question marks.
 */
public class AudioRecognize {
	public static String name;
	public static  String password;

	public static void execute() throws IOException, InterruptedException{
		SpeechToText service = new SpeechToText();
    	
    	
		/*=================================================================================================*/
    	//2018/1/13 Free for a month and maxium up tp 100 minutes
    	//service.setUsernameAndPassword(ACCOUNT,PASSWARD);
		//0f75d45f-4317-4661-b2e9-dcd51643e411  / Wdmol5gWyIRA 備用1
		//String Username = "dd65e81a-813f-4a65-b796-a65007b10592";
		//String passWord = "jBFlSxO2cGha";
		try {
			service.setUsernameAndPassword(name, password);
		}catch (Exception ex){
			LoginIBMPage.Login = false;
			System.out.println("cannot find this ID!");
		}
		/*=================================================================================================*/
		
    	
    	
    	ConverterTest convert = new ConverterTest();
		File audio = new File(convert.getPath());
		//File audio = new File("E:\\1061\\Java\\FinalProject\\test.mp3");
		// final ArrayList<SpeechResults> temp = new ArrayList<SpeechResults>();

		//setting Recognize Options
		RecognizeOptions options = new RecognizeOptions.Builder().model("en-US_BroadbandModel").contentType("audio/mp3")
				.interimResults(false)
				// .maxAlternatives(3)
				// .wordAlternativesThreshold(0.3)
				// .maxAlternatives(10)
				// .wordConfidence(true)
				// .speakerLabels(true)
				.timestamps(true).build();	

		//recognize
		SpeechResults transcript = service.recognize(audio, options).execute();
		
		//Parse Transcript to subtitle
		JSONParser parser = new JSONParser();
		//System.out.println(transcript.getResults().get(0));
		Script tempScript;
		Subtitle full = new Subtitle();
		//get each json from recognize result
		for (Transcript temp : transcript.getResults()) {
			
			try {
				//transform String to Json Object
				Object obj = parser.parse(temp.toString());
				JSONObject jsonObject = (JSONObject) obj;
				
				//get value of key "alternatives"  
				JSONArray alternatives= (JSONArray) parser.parse(jsonObject.get("alternatives").toString());
				
				//all of information timestamp, transcript
				JSONObject tra = (JSONObject) parser.parse(alternatives.get(0).toString());
				
				//2D array
				JSONArray time= (JSONArray) parser.parse(tra.get("timestamps").toString());
				JSONArray timeS= (JSONArray) parser.parse(time.get(0).toString());
				JSONArray timeE= (JSONArray) parser.parse(time.get(time.size()-1).toString());
				
				tempScript = new Script(timeS.get(1).toString(),timeE.get(2).toString(),tra.get("transcript").toString());
				full.add(tempScript);
				
				/*System.out.println("time: " + timeS.get(1));	            
				System.out.println("timeLast: " + timeE.get(2));
				System.out.println("transcript: " + tra.get("transcript"));*/
				//System.out.println(temp);
				

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		//SaveTheShareData.setTextArea(full.getSubtitle());;
		FileUpload.IBMText.setText(full.getSubtitle());
		audio.delete();
		return;
	}
}
