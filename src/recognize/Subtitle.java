package recognize;

import java.util.ArrayList;

public class Subtitle {
	private ArrayList<Script> subtitle = new ArrayList<Script>();
	
	public void add(Script script){
		subtitle.add(script);
		return;
	}
	
	public void print(){
		for(int i=0;i<subtitle.size();i++){
			System.out.println(subtitle.get(i));
		}
		return;
	}
	
	public String getSubtitle(){
		String all="";
		for(int i=0;i<subtitle.size();i++){
			if(i==(subtitle.size()-1))
				all+=subtitle.get(i);
			else
				all+=subtitle.get(i)+"\n";
		}
		return all;
	}
}
