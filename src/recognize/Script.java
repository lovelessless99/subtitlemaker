package recognize;

import java.util.StringTokenizer;

public class Script {
	
	private String start_time;
	private String end_time;
	private String transcript;
	
	public Script(String start_time,String end_time,String transcript)
	{
		this.start_time = formatSeconds(start_time);
		this.end_time = formatSeconds(end_time);
		this.transcript = transcript;
		return;
	}
	
	public void setStartTime(String start_time)
	{
		this.start_time = start_time;
		return;
	}
	
	public void setEndTime(String end_time)
	{
		this.end_time = end_time;
		return;
	}
	
	public void setTranscript(String transcript)
	{
		this.transcript = transcript;
		return;
	}
	
	public String getTranscript()
	{
		return this.transcript;
	}
	
	public String getStartTime()
	{
		return this.start_time;
	}
	
	public String getEndTime()
	{
		return this.end_time;
	}
	@Override
	public String toString()
	{
		return this.start_time+this.transcript;
	}
	
	private String formatSeconds(String timeInSeconds){
		String[] parts = timeInSeconds.split("\\.");
				
		int second = (int) Integer.parseInt(parts[0]);
	    int secondsLeft = second % 3600 % 60;
	    int minutes = (int) Math.floor(second % 3600 / 60);
	    int ms = (int) Integer.parseInt(parts[1]);
	  
	    String MM =String.valueOf((minutes < 10 ? "0" + minutes : minutes));
	    String SS =String.valueOf(secondsLeft < 10 ? "0" + secondsLeft : secondsLeft);
	    String MS =String.valueOf(ms < 10 ? "0" + ms : ms);
	    return  "[" + MM + ":" + SS+"."+ MS + "]";
	}
		
}
