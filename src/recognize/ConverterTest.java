package recognize;

import java.io.*;

import subtitleMaker.FileUpload;
import subtitleMaker.FileUpload.*;
//import java.util.*;
//import java.util.logging.Level;
//import java.util.logging.Logger;



public class ConverterTest{
	  //private static final Logger Log = Logger.getLogger(ConverterTest.class.getName());
	private String MP3;
	public ConverterTest() throws IOException, InterruptedException{
	    //scanner for the mp4 and mp3 file paths
	     // Scanner sc = new Scanner(System.in);
	      
	      //System.out.print("mp4 path: ");
	      String MP4 = FileUpload.communicationWithIBM.getAbsolutePath();
	      this.MP3 = MP4.replace('4', '3');
		  videoConvert(MP4,this.MP3);
		  //command line process
	  }
	  
	public  void videoConvert(String MP4,String MP3) throws IOException, InterruptedException {
		  String line;
		  String cmd = "ffmpeg -i " +MP4+ " "+ this.MP3;
	      Process p= Runtime.getRuntime().exec(cmd); //uses the ffmpeg library to convert
	      BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	      while((line=in.readLine())!=null)//while loop to make sure conversion occurs successfully
	      {
	        System.out.println(line);
	      }
	      p.waitFor();
	      System.out.println("Video Converted");
	      in.close();
	  }
	  
	  public String getPath(){
		 return this.MP3;
	  }
	  
	}
