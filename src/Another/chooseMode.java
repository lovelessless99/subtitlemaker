package Another;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.effect.Bloom;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class chooseMode  implements Initializable {

    @FXML
    private StackPane message;

    @FXML
    private AnchorPane ccBackground;

    @FXML
    private AnchorPane information;

    @FXML
    private Text titleText;

    @FXML
    private AnchorPane background;

    @FXML
    private Text microText;

    @FXML
    private Text ccText;

    @FXML
    private AnchorPane microphone;

    @FXML
    private AnchorPane close;

    @FXML
    private AnchorPane subtitleIcon;

    @Override
    public void initialize (URL url , ResourceBundle resourceBundle ) {
        Font font = Font.font("System",FontWeight.BOLD,39);
        microText.setFont(font);
        ccText.setFont(font);
        titleText.setFont(Font.font("System", FontWeight.BOLD, 43));
    }

    @FXML
    private void textHoverBloom(){
        microText.setFill(Color.web("#FFDA44"));
        microphone.setEffect(new Bloom(0.3));
    }

    @FXML
    private void textLeaveEffect(){
        microText.setFill(Color.WHITE);
        microText.setEffect(null);
        microphone.setEffect(null);
    }

    @FXML
    private void textHoverBloomCC(){
        ccText.setFill(Color.web("#46B29D"));
        subtitleIcon.setEffect(new Bloom(0.3));
    }

    @FXML
    private void textLeaveEffectCC(){
        ccText.setFill(Color.WHITE);
        subtitleIcon.setEffect(null);
    }

    @FXML
    private void closeWindow(){
        Platform.exit();
    }

    @FXML
    private void showInformation() {
        message.toFront();
        Text title = new Text("使用提示");
        Text context = new Text("選擇語音辨識模式，請先向IBM申請一隻帳號，才能使用!" +
                "\n而動態字幕模式，則需自行準備動態歌詞符合相對應的格式");
        title.setFont(Font.font(30));
        title.setFill(Color.BLACK);

        context.setFont(Font.font(25));
        context.setFill(Color.BLACK);

        JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();
        JFXButton jfxButton = new JFXButton("我知道了");
        jfxButton.setMaxSize(70,50);
        jfxButton.getStyleClass().add("buttonnew");
        jfxButton.setButtonType(JFXButton.ButtonType.RAISED);
        jfxButton.setCursor(Cursor.HAND);
        jfxDialogLayout.setHeading(title);
        jfxDialogLayout.setBody(context);
        jfxDialogLayout.setActions(jfxButton);

        JFXDialog jfxDialog = new JFXDialog( message, jfxDialogLayout, JFXDialog.DialogTransition.CENTER);
        jfxDialog.setMaxSize(200,100);
        jfxButton.setOnAction(event -> {
                    message.toBack();
                    message.toBack();
                    message.toBack();
                    jfxDialog.close();
                }
        );
        jfxDialog.show();
    }



}
