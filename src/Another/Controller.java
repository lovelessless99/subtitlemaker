package Another;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.fxml.Initializable;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Text text;

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane subtitleMaker;


    @FXML
    private Polygon light;

    private Boolean isClick = false;
    private Stage primaryStage;
    private static double xOffset = 0;
    private static double yOffset = 0;

    @Override
    public void initialize ( URL url , ResourceBundle resourceBundle ) {
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(5.0);
        text.setEffect(dropShadow);
    }

    @FXML
    private void mouseHover() {
        if( !isClick )  {
            subtitleMaker.getStyleClass().clear();
            subtitleMaker.getStyleClass().add("movie-hover");
            subtitleMaker.getStyleClass().add("image");
            subtitleMaker.setEffect(new Bloom(0.3));
            subtitleMaker.setCursor(Cursor.HAND);
        }
    }

    @FXML
    private  void mouseLeave(){
        if (!isClick) {
            subtitleMaker.getStyleClass().clear();
            subtitleMaker.getStyleClass().add("movie");
            subtitleMaker.getStyleClass().add("image");
            subtitleMaker.setEffect(null);
            subtitleMaker.setCursor(Cursor.DEFAULT);
        }
    }

    @FXML
    private  void playTheAnimation(){
        mouseHover();
        isClick = true;
        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setNode(subtitleMaker);

        translateTransition.setToX(subtitleMaker.getMinWidth()-150);
        translateTransition.setDuration(Duration.millis(2000));

        Timeline timeline = new Timeline();
        KeyFrame keyFrame = new KeyFrame( Duration.millis(0), event -> translateTransition.play() ); //前面的duration是多久才開始執行
        KeyFrame keyFrame1 = new KeyFrame(Duration.millis(1500),event -> { openTheLight() ; textSpread() ; } );
        timeline.getKeyFrames().addAll(keyFrame, keyFrame1 );
        timeline.play();
    }

    @FXML
    private void closeTheWindow(){
        alertWindow();
        //Platform.exit();
    }

    @FXML
    private  void drag(MouseEvent event){
        primaryStage =(Stage) ((Node)event.getTarget()).getScene().getWindow(); //重要********
        primaryStage.setX(event.getScreenX() + xOffset);
        primaryStage.setY(event.getScreenY() + yOffset);
    }
    @FXML
    private void press(MouseEvent event){
        primaryStage = (Stage) ((Node)event.getTarget()).getScene().getWindow();//重要*********
        //System.out.println(event.getTarget());
        Scene scene =  ((Node) event.getTarget()).getScene();
        scene.setCursor(Cursor.HAND);
        xOffset = primaryStage.getX() - event.getScreenX();
        yOffset = primaryStage.getY() - event.getScreenY();
    }

    @FXML
    private void release(MouseEvent event){
        Scene scene = ((Node) event.getTarget()).getScene();
        scene.setCursor(Cursor.DEFAULT);
    }

    private void openTheLight(){
        light.setEffect(new Bloom(0.3));
        FillTransition fillTransition = new FillTransition();
        fillTransition.setShape(light);
        fillTransition.setDuration(Duration.millis(3000));
        fillTransition.setToValue(Color.web("#FFDA44"));
        fillTransition.play();
    }

    private void textSpread(){
        FillTransition fillTransition = new FillTransition();
        fillTransition.setShape(text);
        fillTransition.setDuration(Duration.millis(3000));
        fillTransition.setToValue(Color.BLACK) ;
        fillTransition.play();
    }

    public void alertWindow(){
        stackPane.toFront();
        Text text1 = new Text("提示");
        Text cancel = new Text("取消");
        Text leave = new Text("離開");
        Text contentText = new Text("                 你確定要離開嗎?");

        text1.setFont(Font.font(30));
        contentText.setFont(Font.font(25));

        text1.setFill(Color.WHITE);
        contentText.setTextAlignment(TextAlignment.CENTER);
        contentText.setFill(Color.WHITE);

        JFXDialogLayout content = new JFXDialogLayout();
       content.getStyleClass().add("alertBox");

        JFXDialog jfxDialog = new JFXDialog(stackPane , content  ,JFXDialog.DialogTransition.CENTER);
        JFXButton buttonClose = new JFXButton(leave.getText());

        buttonClose.setOnAction(event -> {
            Platform.exit();
        });

        JFXButton buttonCancel = new JFXButton(cancel.getText());
        buttonCancel.setOnAction(event -> {
                stackPane.toBack();
                stackPane.toBack();
                stackPane.toBack();
            jfxDialog.close();
            }
        );

        buttonCancel.getStyleClass().add("button");
        buttonClose.getStyleClass().add("button");

        buttonClose.setCursor(Cursor.HAND);
        buttonCancel.setCursor(Cursor.HAND);
        content.setHeading(text1);
        content.setBody(contentText);
        content.setActions(buttonClose, buttonCancel);
        jfxDialog.show();
    }
}
