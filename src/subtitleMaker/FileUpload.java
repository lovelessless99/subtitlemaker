package subtitleMaker;

import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.controls.events.JFXDialogEvent;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.Glow;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.Duration;
import recognize.AudioRecognize;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUpload implements Initializable {

    /*******************************   UploadFileTab   *********************************************/

    @FXML
    private Tab uploadFileTab;

    @FXML
    private AnchorPane innerAnchor; // 把檔案拖曳進去時會產生虛線

    @FXML
    private JFXButton fileButton;  // 影片檔案的瀏覽按鈕

    @FXML
    private JFXButton confirmVideoFileButton;  // 確定好檔案之後，跳到 chooseMode 分頁

    @FXML
    private MediaView browseFileVideo;  // 瀏覽上傳影片

    /*********************************   chooseModeTab    ***************************************/

    @FXML
    private Tab chooseModeTab;

    @FXML
    private JFXCheckBox speechCheckBox; // 選擇語音辨識的checkBox

    @FXML
    private JFXCheckBox subtitleCheckBox; // 選擇自己加動態字幕的checkBox

    @FXML
    private JFXButton confirmModeButton; // 確定好模式後，跳到 lyricUploadTab 分頁

    @FXML
    private JFXProgressBar speechProgressBar; // 辨識的進度條

    @FXML
    private Label speechShowMessage; // 辨識訊息

    /*********************************   lyricUploadTab    ***************************************/

    @FXML
    private Tab lyricUploadTab;

    @FXML
    private AnchorPane dashArea; // 拖曳進來的虛線

    @FXML
    private AnchorPane tipBulb; // 提示燈泡

    @FXML
    private JFXTextArea subtitleFromSRT; // 上傳的字幕檔案文字區塊 ----------------------------------------和IBM溝通的橋樑

    @FXML
    private JFXButton browseLyric; // 瀏覽 .srt .txt 檔案的按鈕

    @FXML
    private JFXButton checkGrammer; // 檢查文字區塊內上傳的檔案有沒有符合正規格式的按鈕

    @FXML
    private JFXButton confirmSRTButton; // 確定好有符合格式並前往subtitleTab 分頁

    @FXML
    private Label checkMessage; // 檢查格式時的訊息提示

    /*********************************   subtitleTab    ***************************************/

    @FXML
    private Tab subtitleTab;

    @FXML
    private MediaView modifyMovie; //上傳的影片檔

    @FXML
    private JFXSlider timeSlider; // 影片時間軸

    @FXML
    private AnchorPane playButton; // 播放按鈕

    @FXML
    private Label currentLabel; // 顯示目前播放時間

    @FXML
    private JFXTreeTableView subtitleModifyTable; // 修改歌詞的表格

    @FXML
    private Label subtitleLabel; // 顯示目前播放歌詞壓在影片上( 粗略預覽作用 )

    /*********************************   checkSubtitleTab    ***************************************/

    @FXML
    private Tab checkSubtitleTab;

    @FXML
    private JFXTextArea finalSubtitleText; // 顯示最後檢查完成歌詞

    /*********************************   downloadTab    ***************************************/

    @FXML
    private Tab downloadTab;

    @FXML
    private JFXProgressBar subtitleProgressBar; // 顯示壓字幕下載時的進度

    @FXML
    private JFXProgressBar SRTProgressBar; // 顯示下載歌詞進度

    /*********************************   globalTab(整個)    ***************************************/

    @FXML
    private JFXTabPane TabPane; //

    @FXML
    private StackPane showMessage; // 所有訊息共用這個StackPane

    /*****************************************************  程式的共用變數   ********************************************************/

    private JFXTreeTableColumn<subtitle,String> timeColumn; // 表格時間欄位
    private JFXTreeTableColumn<subtitle,String> lyricColumn; // 表格歌詞欄位

    // 移動整個視窗需要
    private Stage primaryStage;
    private static double xOffset = 0;
    private static double yOffset = 0;

    // 程式內播放影音的物件
    private Media media;
    private MediaPlayer mediaPlayer;  // browseFileVideo MediaView
    private MediaPlayer mediaPlayer2; // modifyMovie  MediaView

    //共用的上傳檔案
    private File videoDownloadSituation;

    // 控制影片
    private boolean stopRequested = false; // 控制播放與暫停
    private boolean atEndOfMedia = false;
    private  Duration duration; // 影片時間

    //動態字幕專用
    private  LyricSplit lyricSplit;
    private  String nextWord = null ;
    private StringBuilder stringBuilder;

    // IBM-speech 溝通用
    public static JFXTextArea IBMText;
    public static File communicationWithIBM; // 影片檔和 IBM- speech 的 工作聯繫 *******************************************************************

    /********************************       初始化         *****************************************/
    @Override
    public void initialize (URL url , ResourceBundle resourceBundle ) {
            IBMText = subtitleFromSRT;
            timeSlider.setValue(0);
            subtitleFromSRT.textProperty().addListener( (obs, ov, nv) -> {
                checkGrammer.setDisable(false);
                checkMessage.setText(null);
            } ); // 設定上傳字幕時，如果一有變化，馬上重新檢查語法
            initializeModifyTable(); // 表格初始化
    }

    private void initializeModifyTable(){
        timeColumn = new JFXTreeTableColumn<>("time");
        timeColumn.setPrefWidth(156);
        timeColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<subtitle, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<subtitle, String> param) {
                return param.getValue().getValue().time;
            }
        });

        lyricColumn = new JFXTreeTableColumn<>("lyric");
        lyricColumn.setPrefWidth(312);

        lyricColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<subtitle, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<subtitle, String> param) {
                return param.getValue().getValue().lyric;
            }
        });

        // 將歌詞欄位變成可以修改的
        lyricColumn.setCellFactory( (TreeTableColumn<subtitle,String> e) ->  new GenericEditableTreeTableCell<>(new TextFieldEditorBuilder()));
        lyricColumn.setOnEditCommit(new javafx.event.EventHandler<TreeTableColumn.CellEditEvent<subtitle, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<subtitle, String> event) {
                TreeItem<subtitle> currentSubtitle = subtitleModifyTable.getTreeItem(event.getTreeTablePosition().getRow());
                currentSubtitle.getValue().setLyric(event.getNewValue());
            }
        });
        //加入欄位
        subtitleModifyTable.getColumns().setAll(timeColumn, lyricColumn);
    }

    //作為表格的欄位物件
    class subtitle extends RecursiveTreeObject<subtitle> {
        StringProperty time;
        StringProperty lyric;

        subtitle(String time, String lyric){
            this.time = new SimpleStringProperty(time);
            this.lyric = new SimpleStringProperty(lyric);
        }

        void setLyric(String newText){
            this.lyric = new SimpleStringProperty(newText);
        }
    }

    /********************************    移動整個視窗   **********************************************/
    @FXML
    private  void drag(MouseEvent event){ //將程式做移動
        primaryStage =(Stage) ((Node)event.getTarget()).getScene().getWindow();
        primaryStage.setX(event.getScreenX() + xOffset);
        primaryStage.setY(event.getScreenY() + yOffset);
    }

    @FXML
    private void press(MouseEvent event){
        primaryStage = (Stage) ((Node)event.getTarget()).getScene().getWindow(); // 得到滑鼠事件的stage(整個程式)
        Scene scene =  ((Node) event.getTarget()).getScene();
        scene.setCursor(Cursor.HAND);
        xOffset = primaryStage.getX() - event.getScreenX();
        yOffset = primaryStage.getY() - event.getScreenY();
    }

    @FXML
    private void release(MouseEvent event){
        Scene scene = ((Node) event.getTarget()).getScene();
        scene.setCursor(Cursor.DEFAULT); // 滑鼠釋放時，將滑鼠恢復原狀
    }

    /*******************************   滑鼠拖曳檔案事件   *********************************************/

    /*Internal and outer communication */
    @FXML
    private void handleDragOver(DragEvent event) {
        innerAnchor.getStyleClass().add("custom-dashed-border");  // 拖曳進去程式的時候，會出現虛線的樣式
        if( event.getDragboard().hasFiles() ) {
            event.acceptTransferModes(TransferMode.ANY);
            clearTheMedia(); // 將前面的mediaPlayer全部清空
        }
    }

    @FXML //拖曳文字檔案
    private void dragOverText(DragEvent event) {
        if( event.getDragboard().hasFiles() ) {
            event.acceptTransferModes(TransferMode.ANY);
            dashArea.getStyleClass().add("custom-dashed-border");
        }
    }

    //滑鼠將檔案放到程式裡面觸發的事件
    @FXML
    private void handleDrop(DragEvent event) {
        List<File> videoFiles;
        innerAnchor.getStyleClass().clear(); // 將虛線清空
        event.getDragboard().getFiles().clear(); // 先將前面的檔案清空
        videoFiles = event.getDragboard().getFiles();

        if (judgeMp4(videoFiles.get(0))) {
            // 設定影片
            communicationWithIBM = videoFiles.get(0);                           // IBM-speech
            media = new Media(videoFiles.get(0).toURI().toString());
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer2 = new MediaPlayer(media);
            modifyMovie.setMediaPlayer(mediaPlayer2);
            browseFileVideo.setMediaPlayer(mediaPlayer);

            fileButton.setText(videoFiles.get(0).getName()); // 將按鈕的名字改掉
            confirmVideoFileButton.setDisable(false);
            setMediaPlayerEvent(); // 設定影片所有事件( 播放 暫停 ... )
            setTimeSlider(); // 設定時間軸
        }

    }
    @FXML
    private void handleDropText (DragEvent event) throws IOException {
        List<File> files;
        event.getDragboard().getFiles().clear(); // 清掉檔案
        dashArea.getStyleClass().clear();          // 清掉虛線

        confirmSRTButton.setDisable(true);
        files = event.getDragboard().getFiles();

        if ( judgeIsTxt(files.get(0)) )
            fileTextToArea(files.get(0));
        else
            notTextFileMessage();

    }

    @FXML //拖曳離開時，虛線也會消失
    private void eliminateDash() {
        innerAnchor.getStyleClass().clear();
    }

    private void clearTheMedia(){
        if( mediaPlayer != null || mediaPlayer2 != null) {
            mediaPlayer2.pause();
            mediaPlayer.pause();
            modifyMovie.setMediaPlayer(null);
            browseFileVideo.setMediaPlayer(null);
            // 暫停兩個音樂後，再將物件設為空
        }
    }

    // 文字檔輸入文字區塊
    private void fileTextToArea(File targetFile) throws IOException{
        confirmSRTButton.setDisable(true);
        BufferedReader br = new BufferedReader(new FileReader(targetFile.getAbsolutePath()));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append("\n");
            line = br.readLine();
        }
        subtitleFromSRT.setText(sb.toString());
        if( subtitleFromSRT.getText() != null)
            checkGrammer.setDisable(false);
    }

    @FXML
    //判斷是否為TXT檔案
    private Boolean judgeIsTxt(File dragFile){
        String fileExtension = dragFile.getName().substring( dragFile.getName().lastIndexOf(".") + 1);
        return  fileExtension.equals("txt") ;
    }

    //判斷是否為Mp4檔案
    private Boolean judgeMp4(File dragFile){
        String fileExtension = dragFile.getName().substring( dragFile.getName().lastIndexOf(".") + 1);
        return  fileExtension.equals("mp4") ;
    }
    /***********************************************控制subtitleTab 預覽畫面的影片以及滑動軸 **************************************************/
    private void setMediaPlayerEvent(){
        mediaPlayer2.currentTimeProperty().addListener(new InvalidationListener() {
            public void invalidated(Observable ov) { updateValues(); }
        });

        mediaPlayer2.setOnReady(new Runnable() {
            public void run() {
                duration = mediaPlayer2.getMedia().getDuration(); // 影片時間
                updateValues();
            }
        });
        mediaPlayer2.setOnPlaying(new Runnable() {
            public void run() {
                if (stopRequested) {
                    mediaPlayer2.pause(); //暫停時觸發事件，設成play鍵
                    stopRequested = false;
                }
                else{
                    playButton.getStyleClass().clear();
                    playButton.getStyleClass().addAll("pause");
                    playButton.getStyleClass().addAll("image");
                }
            }
        });

        //暫停時，將圖案設為播放鍵
        mediaPlayer2.setOnPaused(new Runnable() {
            public void run() {
                playButton.getStyleClass().clear();
                playButton.getStyleClass().addAll("play");
                playButton.getStyleClass().addAll("image");
            }
        });

    }

    //使slider 連接上player，影片可以連結進度條跑
    private void setTimeSlider() {
        // 如果slider 值改變，影片也會到相對應位置的事件
        InvalidationListener sliderChangeListener = o-> {
            if( timeSlider.isValueChanging() ) {
                Duration seekTo = duration.multiply(timeSlider.getValue() / 100.0);
                mediaPlayer2.seek(seekTo);
            }
        };
        timeSlider.valueProperty().addListener(sliderChangeListener);
    }

    // 預覽畫面的字幕
    private void updateValues() {
        if (currentLabel != null && timeSlider != null ) {
            Platform.runLater(new Runnable() {
                public void run() {
                    if (lyricSplit != null) {
                        Duration currentTime = mediaPlayer2.getCurrentTime();
                        int search = currentTime.toString().indexOf(".");
                        double searchTime = Double.parseDouble((currentTime.toString()).subSequence(0, search).toString());
                        Boolean findTime = lyricSplit.findLyric(searchTime);
                        if (findTime) {
                            subtitleLabel.setText(nextWord);
                            int index = 0;
                            for( int i = 0 ; i < timeColumn.getTreeTableView().getExpandedItemCount(); i++)
                            {
                                if(lyricSplit.compareCurrentTime(timeColumn.getCellData(i),searchTime))
                                {
                                    index = i;
                                    break;
                                }
                            }
                            nextWord = lyricColumn.getCellData(index);
                            subtitleModifyTable.requestFocus();
                            subtitleModifyTable.getSelectionModel().select(index);
                            subtitleModifyTable.getFocusModel().focus(index);
                            subtitleModifyTable.scrollTo(index);

                            int length = nextWord.lastIndexOf(" ");
                            subtitleLabel.setPrefHeight( (length >40)?  80 : 39 );
                            subtitleLabel.setLayoutY( (length > 40)? 192 : 232);
                        }

                        subtitleLabel.setText(nextWord);
                        currentLabel.setText(formatTime(currentTime, duration));

                        timeSlider.setDisable(duration.isUnknown());
                        if (!timeSlider.isDisabled()
                                && duration.greaterThan(Duration.ZERO)
                                && !timeSlider.isValueChanging()) {
                            timeSlider.setValue(currentTime.divide(duration).toMillis()
                                    * 100.0);
                        }
                    }
                }
            });
        }
    }

    //將目前時間格式化
    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int)Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
                - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int)Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 -
                    durationMinutes * 60;
            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds,durationMinutes,
                        durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d",elapsedMinutes,
                        elapsedSeconds);
            }
        }
    }

    //撥放鍵的事件設定
    @FXML
    private void stop( ) {
        MediaPlayer.Status status = mediaPlayer2.getStatus();

        if (status == MediaPlayer.Status.UNKNOWN  || status == MediaPlayer.Status.HALTED)
            return;


        if ( status == MediaPlayer.Status.PAUSED
                || status == MediaPlayer.Status.READY
                || status == MediaPlayer.Status.STOPPED)
        {
            // rewind the movie if we're sitting at the end
            if (atEndOfMedia) {
                mediaPlayer2.seek(mediaPlayer2.getStartTime());
                atEndOfMedia = false;
            }
            mediaPlayer2.play();
        }

        else {
            mediaPlayer2.pause();
        }

    }

    /************************************   按鈕發亮事件 ( 滑鼠移進就發亮，移出就不亮 ，Hover 的效果)**************************************************************************************************/

    @FXML
    private void bulbLight() {
        tipBulb.setEffect(new Bloom(0.3));
    } // 提示燈泡

    @FXML
    private void playEffect( ) {
        playButton.setEffect(new Glow(0.9));
    }

    @FXML
    private void bulbDark() {
        tipBulb.setEffect(null);
    } // 音樂撥放鍵

    @FXML
    private void playLeaveEffect( ) {
        playButton.setEffect(null);
    }

    /*******************************************************************使用按鈕選擇檔案*****************************************************/

    //用按鈕上傳影音檔
    @FXML
    private void chooseVideoFile() {
        File videoChoose;

        // 檔案選擇對話盒
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("chooseVideo");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("mp4", "*.mp4"));
        videoChoose = fileChooser.showOpenDialog(new Stage());

        if( videoChoose != null && judgeMp4(videoChoose)){
            confirmVideoFileButton.setDisable(false);
            clearTheMedia();

            // 設定影音
            communicationWithIBM = videoChoose;
            media = new Media(videoChoose.toURI().toString());
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer2 = new MediaPlayer(media);
            browseFileVideo.setMediaPlayer(mediaPlayer);
            modifyMovie.setMediaPlayer(mediaPlayer2);

            // 設定按鈕 以及 預覽影片觸發事件
            String fileName = videoChoose.getName();
            fileButton.setText(fileName);
            setMediaPlayerEvent();
            setTimeSlider();
        }
    }

    //用按鈕上傳文字檔
    @FXML
    private void uploadTextFile() throws IOException{
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        fileChooser.setTitle("chooseLyric");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("txt", "*.txt"));
        File file = fileChooser.showOpenDialog(stage);
        if(judgeIsTxt (file))
            fileTextToArea(file);
        else
            notTextFileMessage();
    }

    /******************************************************************    所有提示訊息   (需要縮減)  **********************************************************************/

    private void errorMessage(Text head, Text body, Text Button, double headFontSize, double bodyFontSize){
        showMessage.toFront();

        /*請要用ANSII的編碼格式*/
        head.setFont(Font.font(headFontSize));
        head.setFill(Color.BLACK);

        body.setFont(Font.font(bodyFontSize));
        body.setTextAlignment(TextAlignment.CENTER);
        body.setFill(Color.BLACK);

        JFXDialogLayout content = new JFXDialogLayout();
        JFXDialog jfxDialog = new JFXDialog(showMessage , content  ,JFXDialog.DialogTransition.CENTER);
        jfxDialog.setOnDialogClosed(e->{
                    showMessage.toBack();
                }
        );

        JFXButton buttonCancel = new JFXButton(Button.getText());
        buttonCancel.setOnAction(event -> {
            jfxDialog.close();
            showMessage.toBack();
            }
        );

        buttonCancel.setTextFill(Color.BLACK);
        buttonCancel.setCursor(Cursor.HAND);
        content.setHeading(head);
        content.setBody(body);
        content.setActions(buttonCancel);
        jfxDialog.show();
    }
    @FXML
    private void closeWindow() {
        showMessage.toFront();
        Text text1 = new Text("提示");
        Text cancel = new Text("取消");
        Text leave = new Text("離開");
        Text contentText = new Text("              你確定要離開嗎?");

        text1.setFont(Font.font(30));
        contentText.setFont(Font.font(25));

        text1.setFill(Color.WHITE);
        contentText.setTextAlignment(TextAlignment.CENTER);
        contentText.setFill(Color.WHITE);

        JFXDialogLayout content = new JFXDialogLayout();
        content.getStyleClass().add("alarm");

        JFXDialog jfxDialog = new JFXDialog(showMessage , content  ,JFXDialog.DialogTransition.CENTER);
        jfxDialog.setOnDialogClosed(new EventHandler<JFXDialogEvent>() {
            @Override
            public void handle(JFXDialogEvent event) {
                showMessage.toBack();
                showMessage.toBack();
            }
        });
        JFXButton buttonClose = new JFXButton(leave.getText());

        buttonClose.setOnAction(event -> {
            Platform.exit();
        });

        JFXButton buttonCancel = new JFXButton(cancel.getText());
        buttonCancel.setOnAction(event -> {
            showMessage.toBack();
            showMessage.toBack();
            showMessage.toBack();
                    jfxDialog.close();
                }
        );

        buttonClose.setTextFill(Color.WHITE);
        buttonCancel.setTextFill(Color.WHITE);

        buttonClose.setCursor(Cursor.HAND);
        buttonCancel.setCursor(Cursor.HAND);
        content.setHeading(text1);
        content.setBody(contentText);
        content.setActions(buttonClose, buttonCancel);
        jfxDialog.show();
    }

    @FXML
    private void uploadFileTip(){
        Text head = new Text("上傳提示");
        Text leave = new Text("我知道了");
        Text contentText = new Text("您有以下三種方式可以上傳文字 : \n" +
                "一、直接將動態歌詞內容貼上下列文字欄位\n" +
                "二、用下列 BrowseLyric 按鈕選擇 txt 檔案\n" +
                "三、直接將 txt 檔拖移進程式裡面\n"+
                "(※請注意皆要符合動態歌詞格式)\n"
        );
        errorMessage(head, contentText, leave,30,25);
    }

    private void notTextFileMessage(){
        Text head = new Text("檔案類型不符");
        Text leave = new Text("我知道了");
        String content = "不是文字檔 ，請重新上傳 ! ";
        Text contentText = new Text(content);
        errorMessage(head, contentText, leave, 30,25);
    }


    /********************************檢查歌詞是否符合正規表示法******************************/
    @FXML
    private void checkLyric(){
        ArrayList<errorWord> errorSentence = new ArrayList<>(); // 儲存錯誤的句子
        final String REGEX = "\\[[0-9]{2}\\:[0-9]{2}\\.[0-9]{2}\\]([^\\s]+.*|[\\s]*)"; // 正規表示法檢查歌詞
        Pattern pattern = Pattern.compile(REGEX);
        String[] temp = subtitleFromSRT.getText().split("\n"); // 將歌詞做分割
        int count = 0;
        for(int i = 0 ; i < temp.length ; i++ ){
            Matcher matcher = pattern.matcher(temp[i]);
            if(matcher.find()) {
                if(matcher.end() != temp[i].length())
                    highLightError(temp[i], i, errorSentence);
                else
                    count++;
            }
            else{
                highLightError(temp[i], i, errorSentence); // 錯的話將錯的地方上色
            }
        }
        judgeError(errorSentence);
    }

    private void highLightError(String error,int number,ArrayList<errorWord> word){
        word.add(new errorWord(error, number));
        int position = subtitleFromSRT.getText().indexOf(error);
        int offset = error.length();
        subtitleFromSRT.selectRange( position , position + offset); // 對該句子做反白
        subtitleFromSRT.setStyle("-fx-highlight-fill: lightblue; -fx-highlight-text-fill: firebrick; -fx-font-size: 16px;");
    }

    class errorWord{
        String sentence;
        int rowNumber;
        errorWord(String sentence, int rowNumber){
            this.sentence = sentence;
            this.rowNumber = rowNumber;
        }
    }

    // 輸出訊息，有錯會說第幾行有錯
    private void judgeError(ArrayList<errorWord> word){
        if(word.isEmpty()) {
            checkMessage.setText("格式正確 ! 可進行下一個步驟 ! ");
            checkMessage.setTextFill(Color.web("#f2deb3"));
            confirmSRTButton.setDisable(false);
        }
        else{
            StringBuilder stringBuilder = new StringBuilder("格式錯誤 ! 分別在第: ");
            for(errorWord sent : word){
                stringBuilder.append(Integer.toString(sent.rowNumber));
                stringBuilder.append(" ");
            }
            stringBuilder.append("列");
            checkMessage.setText(stringBuilder.toString());
            checkMessage.setTextFill(Color.CORAL);
        }

    }

    /*******************************************************下載功能**************************************************/
    @FXML
    public void downloadLyric() throws IOException {
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();

        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("txt", "*.txt"),
                new FileChooser.ExtensionFilter("srt", "*.srt")
        );

        File fileChoose = fileChooser.showSaveDialog(stage);
        Task SRTTask = new Task() {
            @Override
            protected Object call() throws Exception {
                String lyricPath=stringBuilder.toString();
                String srtPath=fileChoose.toPath().toString();
                FileWriter fw = new FileWriter(srtPath);
                System.out.print(fw.getEncoding());
                fw.write(lyricPath);
                for(int i = 0 ; i < lyricPath.length() ; i++ ){
                    updateProgress(i,lyricPath.length());
                }
                fw.flush();
                fw.close();
                return null;
            }
        };
        SRTProgressBar.progressProperty().unbind();
        SRTProgressBar.progressProperty().bind(SRTTask.progressProperty());
        new Thread(SRTTask).start();
    }

    @FXML
    public void downloadVideo() throws IOException, InterruptedException{
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();

        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("mp4", "*.mp4")
        );

        videoDownloadSituation = fileChooser.showSaveDialog(stage);
        Task task = setTask();
        subtitleProgressBar.progressProperty().unbind();
        subtitleProgressBar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();
    }

    private Task setTask() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                String line;
                String lyricPath = finalSubtitleText.getText();

                FileWriter fw = new FileWriter("Lyric.srt");
                fw.write(lyricPath);
                fw.flush();
                fw.close();

                String cmd="ffmpeg -i "+ mediaPlayer.getMedia().getSource().substring(6) +" -vf \"subtitles=Lyric.srt\" "+ videoDownloadSituation.toPath();
                Process p= Runtime.getRuntime().exec(cmd); //uses the ffmpeg library to convert
                InputStreamReader temp = new InputStreamReader(p.getErrorStream());
                BufferedReader in = new BufferedReader(temp);

                int count = 0;
                int i = 0 ;
                while((line=in.readLine())!=null)   //while loop to make sure conversion occurs successfully
                {
                    i++;
                    System.out.println(line);
                    updateProgress(count++,300);
                }
                updateProgress(300,300);
                p.waitFor();
                in.close();
                File f = new File("Lyric.srt");
                f.delete();
                return true;
            }
        };

    }

    /******************************************其他功能***************************************************/
    //控制checkbox互斥功能
    @FXML
    public void checkBoxControl(ActionEvent event) {
        if (event.getSource() instanceof CheckBox) {
            CheckBox chk = (CheckBox) event.getSource();

            if (" Speech Recognition".equals(chk.getText() ) ) {
                speechCheckBox.setSelected(true);
                subtitleCheckBox.setSelected(false);
            }
            if ("Subtitle Mode".equals(chk.getText())) {
                speechCheckBox.setSelected(false);
                subtitleCheckBox.setSelected(true);
            }
        }
        confirmModeButton.setDisable(false);
    }

    //將上傳的動態歌詞轉成表格
    private void upload() {
        lyricSplit = new LyricSplit(subtitleFromSRT.getText());
        lyricSplit.Split();
        lyricSplit.timeSplit();
        lyricSplit.generateTreeMap();
        ObservableList<subtitle> subtitles = FXCollections.observableArrayList();
        String[] time = lyricSplit.getLyricsplit();
        String[] pureLyric = lyricSplit.getPureLyric();

        for( int i = 0 ; i < time.length ; i++)
            subtitles.add( new subtitle( time[i], pureLyric[i] ) );

        final TreeItem<subtitle> root = new RecursiveTreeItem<>(subtitles, RecursiveTreeObject::getChildren);
        subtitleModifyTable.setEditable(true);
        subtitleModifyTable.setRoot(root);
        subtitleModifyTable.setShowRoot(false);
    }

    //將表格轉成最終確認動態歌詞
    @FXML
    private void tableToLyric(){
        stringBuilder = new StringBuilder();

        for( int i = 0 ; i < timeColumn.getTreeTableView().getExpandedItemCount() ; i++ ) {
            String time = timeColumn.getCellData(i);
            String lyric = lyricColumn.getCellData(i);
            stringBuilder.append("[");
            stringBuilder.append(time);
            stringBuilder.append("]");
            stringBuilder.append(lyric);
            stringBuilder.append(System.getProperty("line.separator"));
        }
        finalSubtitleText.setText(stringBuilder.toString());

        mediaPlayer2.pause();
        checkSubtitleTab.setDisable(false);
        TabPane.getSelectionModel().select(checkSubtitleTab);
    }

    /**************************************** 切換分頁按鈕 ***********************************************/
    @FXML
    private void changeChooseMode(){
        chooseModeTab.setDisable(false);
        TabPane.getSelectionModel().select(chooseModeTab);
    }

    @FXML
    public void changeToLyricTab() throws InterruptedException, IOException {
        lyricUploadTab.setDisable(false);
        if (subtitleCheckBox.isSelected()) {
            TabPane.getSelectionModel().select(lyricUploadTab);
            lyricUploadTab.setDisable(false);
        } else {
            loadLoginPage();
        }
    }
    private void loadLoginPage() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LoginIBMPage.fxml")) ;
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);

        stage.initStyle(StageStyle.UNDECORATED);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.initModality(Modality.APPLICATION_MODAL);

        stage.setScene(scene);
        stage.setOnHiding(event -> {
            if (LoginIBMPage.Login) {
                TabPane.getSelectionModel().select(lyricUploadTab);
                lyricUploadTab.setDisable(false);
            }
        });
        stage.show() ;
    }

    @FXML
    private void changeToSubtitleTab(){
        upload();
        TabPane.getSelectionModel().select(subtitleTab);
        subtitleTab.setDisable(false);
    }

    @FXML
    private void changeToDownload(){
        downloadTab.setDisable(false);
        TabPane.getSelectionModel().select(downloadTab);
    }

    /**************************************** 重新啟動Restart *********************************************/
    @FXML
    private void restart(){
        clearTheMedia();
        timeColumn.getTreeTableView().setRoot(null);

        subtitleProgressBar.progressProperty().unbind();
        subtitleProgressBar.progressProperty().setValue(-1);
        SRTProgressBar.progressProperty().unbind();
        SRTProgressBar.progressProperty().setValue(-1);
        timeSlider.setValue(0);

        subtitleCheckBox.setSelected(false);
        speechCheckBox.setSelected(false);

        finalSubtitleText.setText(null);
        subtitleFromSRT.setText(null);
        checkMessage.setText(null);
        subtitleLabel.setText(null);

        checkGrammer.setDisable(true);
        confirmSRTButton.setDisable(true);
        TabPane.getTabs().forEach(tab -> tab.setDisable(true));
        uploadFileTab.setDisable(false);

        subtitleLabel.setPrefHeight(39);
        TabPane.getSelectionModel().selectFirst();
        fileButton.setText("Browse Files");
    }

}
