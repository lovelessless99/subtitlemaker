package subtitleMaker;

import java.text.DecimalFormat;
import java.util.TreeMap;

public class LyricSplit {

    private TreeMap<Double , String > SRT_Table = new TreeMap() ;

    private double [] timeSlice  = new double[200] ; //算出經過秒數，當時間走到該秒數作為對應
    private String  [] pureLyric  = new String[200]  ; //純歌詞
    private String[] timePart;  //純時間(含括弧)

    private String lyric;           //純歌詞
    private DecimalFormat df = new DecimalFormat("#.#"); //小數點格式化用

    //建構子
    LyricSplit(String textArea){
        this.lyric = textArea;
    }

    //印出此物件的動態歌詞
    public void printOriginal(){
        System.out.println(lyric);
    }

    //把歌詞以及時間做切割(請務必要用英文且用ASCII)
     void Split(){
         timePart = lyric.split("\n");
        for(int i = 0 ; i < timePart.length ; i++) {
            pureLyric[i] = timePart[i].substring(10);
            timePart[i] = timePart[i].substring(1, 9);
        }
    }

    //將時間做切割，切成分跟秒
     void timeSplit(){
        for(int i = 0; i < timePart.length ; i++ )
            timeSlice[i] = timeTransToDouble(timePart[i]);
    }

    //將時間和歌詞生成對應表，利用雜湊找東西效率快
    void generateTreeMap(){
        for(int i = 0 ; i < timePart.length ; i++)
            SRT_Table.put( timeSlice[i] , pureLyric[i] );
    }

    /* 比較進行時間(millisecond)與表格歌詞時間(time)，一樣就回傳 true */
     boolean compareCurrentTime(String time, Double millisecond){
        double tableTime =  timeTransToDouble(time);
        millisecond = formatCurrentTime(millisecond);
        return tableTime == millisecond;
    }

    /* 找到時間就回傳 true*/
     boolean findLyric(Double millisecond) {
        millisecond = formatCurrentTime(millisecond);
        return SRT_Table.containsKey(millisecond);
    }

    //將時間轉成小數點形式
    private double timeTransToDouble(String time){
        double minute =  Double.parseDouble(time.substring(1,2));
        double second = Double.parseDouble(time.substring(3));
        double totalTime =  minute*60 + second;
        totalTime = Double.parseDouble(df.format(totalTime));
        return totalTime;
    }

    //格式化時間
    private double formatCurrentTime(double millisecond){
        millisecond = millisecond / 1000;
        millisecond = Double.parseDouble(df.format(millisecond));
        return millisecond;
    }

    //回傳時間，不包含外面的框框 (作為生成表格用)
     String[] getLyricsplit() {
        return timePart;
    }

    //回傳歌詞(作為生成表格用)
     String[] getPureLyric() {
        return pureLyric;
    }
}
