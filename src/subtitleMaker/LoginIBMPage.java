package subtitleMaker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import recognize.AudioRecognize;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginIBMPage implements Initializable {
    private Stage primaryStage;     //整個程式的stage
    private double xOffset,yOffset; //計算移動的距離
    public  static  Boolean Login = false; // 是否登入了~
    @FXML
    private JFXTextField IBMID;

    @FXML
    private JFXPasswordField password;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    private void browserRegister() throws URISyntaxException{
        try {
            //造訪IBM登入頁面
            Desktop.getDesktop().browse(new URI("https://idaas.iam.ibm.com/idaas/mtfim/sps/authsvc?PolicyId=urn:ibm:security:authentication:asf:basicldapuser&Target=https%3A%2F%2Fwww.ibm.com%2Fwatson%2Fservices%2Fspeech-to-text%2F"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /*拖曳視窗的function*/
    @FXML
    private  void mouseDrag(MouseEvent event){
        primaryStage =(Stage) ((Node)event.getTarget()).getScene().getWindow();
        primaryStage.setX(event.getScreenX() + xOffset);
        primaryStage.setY(event.getScreenY() + yOffset);
    }

    /*滑鼠按壓動作移動視窗*/
    @FXML
    private void mousePress(MouseEvent event){
        primaryStage = (Stage) ((Node)event.getTarget()).getScene().getWindow();
        Scene scene =  ((Node) event.getTarget()).getScene();
        scene.setCursor(javafx.scene.Cursor.HAND);
        xOffset = primaryStage.getX() - event.getScreenX();
        yOffset = primaryStage.getY() - event.getScreenY();
    }

    /*滑鼠釋放，並將滑鼠設回原本樣式*/
    @FXML
    private void mouseRelease(MouseEvent event){
        Scene scene = ((Node) event.getTarget()).getScene();
        scene.setCursor(Cursor.DEFAULT);
    }

    // 登入後開始辨識
    @FXML
    private void closeWindow (MouseEvent event) throws InterruptedException{
        Login = true;
        AudioRecognize.name = IBMID.getText();
        AudioRecognize.password = password.getText();

        Task speechTask = new Task() {
            @Override
            protected Object call() throws Exception {
                AudioRecognize.execute();
                return null;
            }
        };
        Thread task = new Thread(speechTask);
        task.start();
        task.join();
        Stage stage = (Stage) ((Node)event.getTarget()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void closeWindowWithoutLogin(MouseEvent event){
        Login = false;
        Stage stage = (Stage) ((Node)event.getTarget()).getScene().getWindow();
        stage.close();
    }

}
